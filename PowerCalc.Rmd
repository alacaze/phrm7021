---
title: "Common Statistical Tests: Calculating Power"
author: Adam La Caze
date: "2019-08-09"
bibliography: '../local.bib'
output: 
  html_document:
    theme: flatly
    toc: true
    toc_float:
      collapsed: false
#   pdf_document:
#     includes:
#         in_header: ${HOME}/Dropbox/Pandoc/pandoc_DATADIR/templates/RMD_preamble.tex
#     toc: true
#     toc_depth: 2
#     keep_tex: true
# fontsize: 12pt
---

All the examples so have have assumed that the study is complete and we have the data to hand.
An important part of designing a study is to assess statistical power of the planned test.

Let's say that we wanted to conduct a follow up study to the one we analysed in Example 1. 
We are going to conduct the trial in a different population and modify the intervention slightly.
We hypothesise that the intervention will still work, but believe that effect size will be half the size of the original study.

How large would the new study need to be to detect an effect of this size?

The first decision to make is the planned statistical test.
Let's start with the data assumptions we first made about the MAQ scale and use a one-sided t-test.

To determine the required sample size we will use the function, [pwr.t.test()](https://www.rdocumentation.org/packages/pwr/versions/1.2-2/topics/pwr.t.test).

If you read the documentation, you will see we need the following:

* Expected difference between groups
* The pooled standard deviation of the groups
* Significance level
* Desired power

We will stick with $\alpha = 0.05$ and $\beta = 0.2$ (i.e. a power of 80%).

The expected difference and standard deviation are used to calculate a standardized effect size.
The expected difference between groups has been provided, and we can use the previous study to estimate standard deviation.
(You don't always have a previous study to estimate standard deviation, in this case you may be able to estimate it from the literature).

The expected difference is 0.35 and the pooled standard deviation (from the previous study) is (approximately) 1.0.

```{r echo=TRUE, eval=TRUE, message=FALSE, warning=FALSE, tidy=TRUE, results=TRUE, error=FALSE}

library(pwr) # this is package that has the power calculations we are using; you will need to install it

delta <- 0.35 # expected difference
sigma <- 1 # observed standard deviation
d <- delta/sigma

pwr.t.test(d = d, power = 0.8, type = "two.sample", alternative = "greater")

```

You would need to recruit 200 participants to have 80% power to detect the difference we expect between the groups.

# Exercises

5. Let's say the study started and you are worried tha the true side of the effect is a difference of 0.25 rather than 0.35.  What would be the __power__ of the test if your sample size and expected standard deviation stayed the same?